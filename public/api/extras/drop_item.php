<?php


require_once 'include/DB_Functions.php';
$db = new DB_Functions();

// array for final json respone
$response = array();
$parentDirectory = realpath(__DIR__ . '/..');

$latitude 	= $_POST['latitude'];
$longitude 	= $_POST['longitude'];
$sector     = $_POST['sector'];
$userName   = $_POST['user_name'];
$userID     = $_POST['user_id'];
$access     = $_POST['access'];
$friendList = json_decode($_POST['friend_list'], true);
$public     = $_POST['public'];
$itemID		= -1;


$path = $parentDirectory . '/sector/' . $sector ;

//When MKDIR is called, umask comes into to play to limit file permissions.
//We must use chmod after dir or file creation to make sure we can save files.	
if (!file_exists($path) && !is_dir($path)) {

	mkdir($path, true);
	chmod($path, 01777);
} 	 

// getting server ip address
$server_ip = gethostbyname(gethostname());

// final file url that is being uploaded
$file_upload_url = 'http://' . $server_ip . $path;


if (isset($_FILES['image']['name'])) {	

	$fileName   = basename($_FILES["image"]["name"]);
	$location = $path . '/' . $fileName;

	// reading other post parameters
	$email = isset($_POST['email']) ? $_POST['email'] : '';
	$website = isset($_POST['website']) ? $_POST['website'] : '';

	$response['file_name'] = $fileName;
	$response['email'] = $email;
	$response['website'] = $website;

	try {
		// Throws exception incase file is not being moved
		if (!move_uploaded_file($_FILES["image"]["tmp_name"], $location)) 
		{
			// make error flag true
			$response['error'] = true;
			$response['message'] = 'Could not move the file!';			
		}
		else
		{
			$itemID = $db->storeToDatabase($userID, $userName, $latitude, $longitude, $sector, $public, $fileName);
			// File successfully uploaded
			$response['stored'] = $itemID;
			$response['message'] = 'File uploaded successfully!';
			$response['error'] = false;
			$response['file_path'] = $file_upload_url . '/' . basename($_FILES["image"]["name"]);

		}
	} catch (Exception $e) {

		// Exception occurred. Make error flag true
		$response['error'] = true;
		$response['message'] = $e->getMessage();
	}
}
else {
	// File parameter is missing
	$response['error'] = true;
	$response['message'] = 'Not received any file!';
}

if($access == '0')
{
	foreach($friendList['friends'] as $friendID)
	{		

		$stored = $db->storeToItemRelation($itemID, (int)$friendID);				
	}
}

// Echo final json response to client
echo json_encode($response);

?>
