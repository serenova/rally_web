Authentication/Startup
* Check session with android, if not logged in, show options:

    1. User uses Facebook
        a. Retrieve Access Token
        b. Request user info with access token
        c. Send info with Volley to attempt login/register
            1. As extra security, use appsecret_proof, and do one more call to facebook serverside.
            2. Request comes back a success
                a. Enter values you need into SQLite
                b. Enter Main Activity
            3. Request comes back a failure
                a. Report to user the issue, and let the user try again.
        
    2. User uses Google
    
    3. User uses regular sign in
        a. Volley Sign In
        b. If success place info in SQLite
        c. Enter Main Activity
    4. User uses Register
        a. Take info, use Volley to register
        b. If success place info in SQLite
        c. Enter Main Activity