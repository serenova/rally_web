#!/bin/bash
mkdir -p /var/www/rally_web/lib

#Change working directory to lib
pushd /var/www/rally_web/lib

#Install needed libraries
#Encryption All-in-One
composer require defuse/php-encryption
#Send Emails
composer require phpmailer/phpmailer
#Unit Testing
composer require --dev phpunit/phpunit ^6.5

#change back working directory
popd
