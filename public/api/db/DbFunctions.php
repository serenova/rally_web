<?php
//CRUD (Create/Read/Update/Delete) structure for DB communication
//-----------------------------------
//Public directory
$rootDir    =   $_SERVER['DOCUMENT_ROOT'];
//Load Database Connection and Security Functions
require_once("$rootDir"."/api/db/DbConnectPDO.php");

//USER REGEX Expressions for sanitation in a new method
class DbFunctions
{
    //Variables that hold instance of DB_Connect_PDO
    private $db;
    private $db_conn;

    //Arrays the hold all database column names
    private $userColumns = null;
    private $ralliesColumns = null;


    //put your code here
    // constructor
    function __construct() {

        $this->db = DbConnectPDO::getInstance();
        $this->db_conn = $this->db->getConnection();

    }

    // destructor
    function __destruct() {

    }

    function run($sql, $bind=array())
    {
        //Removes trailing white space
        $sql = trim($sql);

        try
        {
            //Send MYSQL the query but have it wait for the arguments with execute()
            $result = $this->db_conn->prepare($sql);

            if($bind==null)
            {
                $result->execute();
            }
            else
            {
                //Sends database the values in order to complete execution
                $result->execute($bind);
            }

            return $result;
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
            return false;
        }
    }
    //Insert entry into database
    function create($table, $data)
    {
        //Verify that the table and data correspond to correct columns
        $fields = $this->verify($table, $data);
        $bind   = array();

        //Fill up the bind array with :key = value
        foreach($fields as $fieldKey => $fieldValue)
        {
            $bind[":$fieldKey"] = $fieldValue;
        }

        //Create INSERT statement for vierified columns and values
        $sql = "INSERT INTO " . $table . " (" . implode(array_keys($fields), ", ") . ") VALUES (:" . implode(array_keys($fields), ", :") . ");";

        //Insert entry
        return $this->run($sql, $bind);

    }

    //Return ALL(*) entries that match the where clause
    //read("users", "email=:email", $bind)  Returns all users with the email given in bind :email => "hamlet@serenova.xyz"
    //Be sure to include fields of which values you want returned, to limit unecessary load
    function read($table, $where="", $bind=array(), $fields="*")
    {

        $sql = "SELECT " . $fields . " FROM " . $table;

        if(!empty($where))
        {
            $sql .= " WHERE " . $where;
        }
        $sql .= ";";

        $query = $this->run($sql, $bind);

        //If query succeeded, get all rows, then return them
        if($query)
        {
            //Add all the rows of the query and return them.
            return $query->fetchAll();
        }

        //Return false if query failed
        return $query;
    }
    //Update values in database
    function update($table, $data, $where, $bind=array())
    {
        //Verify and sanitize(in the future) values and keys
        $fields = $this->verify($table, $data);

        //Begin creating SQL query
        $sql = "UPDATE " . $table . " SET ";

        //Continue filling up the sql query string
        foreach($fields as $fieldKey => $fieldValue)
        {
            $sql .= $fieldKey . " = :update_" . $fieldKey.", ";
            //Fill bind array
            $bind[":update_$fieldKey"] = $fieldValue;
        }
        //This statement makes sure to remove the trailing comma.
        $sql = substr_replace($sql, " ", -2, 2);

        $sql .= " WHERE " . $where . ";";

        //Run query
        return $this->run($sql, $bind);
    }
    function delete($table, $where, $bind="")
    {
        $sql = "DELETE FROM " . $table . " WHERE " . $where . ";";

        return $this->run($sql, $bind);
    }
    /*
    This function checks if the data sent by client for the query match the
    correct column names for the table requested. Then returns the matched column entries (associative array of both keys and values).
     */
    private function verify($table, $data)
    {
        if(($table == null) || ($data == null))
        {
            return false;
        }
        $columns = null;
        switch($table)
        {
        case "users":
            $columns    =   $this->db->getUserColumns();
            break;
        case "rallies":
            $columns    =   $this->db->getRalliesColumns();
            break;
        case "auth_info":
            $columns    =   $this->db->getAuthInfoColumns();
            break;
        default:
            exit(1);
        }
        /*
        The order of the arguments is IMPORTANT.
        Only values of array 1 are transferred to the output
        Returns an array with only the values that match the keys in associative array
        */
        return array_intersect_key($data, $columns);


    }
    function getLastId()
    {
        return $this->db_conn->lastInsertId();
    }
}

?>
