<?php
//Public directory
$rootDir = $_SERVER['DOCUMENT_ROOT'];
//Private directory
$parentDir =   dirname($rootDir);

require_once($parentDir.'/lib/vendor/autoload.php');

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Exception as Ex;
use Defuse\Crypto\File;
use Defuse\Crypto\Key;
use Defuse\Crypto\KeyProtectedByPassword;

function createReturnCode()
{
    $selector   = substr(uniqid(rand(), true), 0, 12);
    $validator  = encrypt($selector);

    return $selector."$".$validator;
}
function loadEncryptionKeyFromConfig()
{
    //May need to hardcode into PHP file depending on speed/performance
    $secretFile =   fopen("/var/www/rally_web/config/rally-return_code-key.txt", "r");

    $keyAscii = fgets($secretFile);

    return Key::loadFromAsciiSafeString($keyAscii);
}
function encrypt($data)
{
    $key = loadEncryptionKeyFromConfig();

    return Crypto::encrypt($data, $key);
}
function decrypt($data)
{

    try {
        $key = loadEncryptionKeyFromConfig();
        return Crypto::decrypt($data, $key);

    } catch (\Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException $ex) {
        // An attack may have been attempted.
    }
}


?>
