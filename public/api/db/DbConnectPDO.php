<?php

class DbConnectPDO
{

    private $_connection;
    private static $_instance; //The single instance

    private $userColumns        = null;
    private $ralliesColumns     = null;
    private $mainKeys           = null;
    private $authInfoColumns    = null;

    /*
    Get an instance of the Database
    @return Instance
    */
    public static function getInstance()
    {
        if (!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    // Constructor
    private function __construct()
    {
        try
        {


            //For security leave DB configurations in INI file.
            $ROOT           =   dirname($_SERVER['DOCUMENT_ROOT']);
            $config         =   parse_ini_file("$ROOT"."/database/database_config.ini");

            //Parse out DB Configurations
            $dbDriver      =   $config["DB_DRIVER"];
            $dbHost        =   $config["DB_HOST"];
            $dbDatabase    =   $config["DB_DATABASE"];
            $dbUser        =   $config["DB_USER"];
            $dbPassword    =   $config["DB_PASSWORD"];
            $dbCharset     =   $config["DB_CHARSET"];

            //Set options
            $options = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => false,
            ];

            $dbSetup = null;

            switch($dbDriver)
            {
                case "sqlite":
                    $dbSetup = "sqlite:$dbDatabase;charset=$dbCharset";
                    break;
                case "mysql":
                    $dbSetup = "mysql:host=$dbHost;dbname=$dbDatabase;charset=$dbCharset";
                    break;
                default:
                    echo "Unsuportted DB Driver! Check the configuration.";
                    exit(1);
            }

            $this->_connection  = new PDO($dbSetup, $dbUser, $dbPassword, $options);

            /*** echo a message saying we have connected ***/
            //echo Connected to database;

            //Parse all column names of all tables
            $tableColumns   =   parse_ini_file("$ROOT"."/database/columns.ini", true);
            $jsonKeys       =   parse_ini_file("$ROOT"."/database/http_keys.ini", true);

            $this->userColumns      = $tableColumns["users"];
            $this->ralliesColumns   = $tableColumns["rallies"];
            $this->authInfoColumns  = $tableColumns["auth_info"];
            $this->mainKeys      = $jsonKeys["main"];
        }
        catch (Exception $e)
        {
            echo $e->getMessage();
            exit(1);
        }
    }
    // Magic method clone is empty to prevent duplication of connection
    private function __clone()
    {
    }
    // Get mysql pdo connection
    public function getConnection()
    {
        return $this->_connection;
    }
    public function getMainKeys()
    {
        return $this->mainKeys;
    }
    public function getUserColumns()
    {
        return $this->userColumns;
    }
    public function getRalliesColumns()
    {
        return $this->ralliesColumns;
    }
    public function getAuthInfoColumns()
    {
        return $this->authInfoColumns;
    }

}
?>
