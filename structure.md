Authentication/Authorization:
----------------------
**Sleek Auth - return_code**
* return_code = selector.validator
    - selector = randomNum()
    - validator = encrypt(selector);
    - Database stores hashed version of validator.
* Info held in session/local storage
* Every http request triggers a DB lookup to determine the user_id
* Session-less REST architecture.

_PROS:_
* Allows individual access to user authentication.
    - Able to redact login access for that particular return_code.
    JWT will invalidate all tokens if encryption key changes.
* Immediate logout capability of the user for any single device currently logged in.
* Selector.validator scheme (validator hashing) helps prevent mass user info hijacking in the event of the DB table info leaking out or being stolen.
* Selector.validator also help prevent timing leaks during DB lookups as opposed to:
    - **SELECT * FROM auth_tokens WHERE token = 'sdfhg384duou8923';**
* Prevents the leakage of the database user_id field which leaks the number of active users on service.


_CONS:_
* Speed?
    - Maybe DB calls to read user_id (using return_code) take longer than decrypting JWTs.
    But this seems like a negligible difference in timing.
