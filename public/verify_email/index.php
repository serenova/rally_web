<?php
//Verify correct arguments are sent
if(!empty($_GET) && !verifyKeys($_GET))
{
    echo "Improper request!";
    return;
}

//Public directory
$rootDir = $_SERVER['DOCUMENT_ROOT'];

require_once("$rootDir" . "/api/auth/AuthFunctions.php");

$auth = new AuthFunctions();
//Check if user email has already been verified

if($auth->isEmailVerified($_GET))
{
    echo "Your email is already verified!";
}
//User email needs to be verified
else
{
    //Ternary operator makes for quick and easy if else code.
    echo $auth->activateAccount($_GET) ? "Email Verified! Enjoy Rally!" : "Unable to Verify Email.";

}

function verifyKeys($data)
{
    $requiredKeys = array("email"=> 1, "return_code"=> 1);

    //Returns true if both required keys are in request, and there is no difference from $requiredKeys and $data
    return !array_diff_key($data, $requiredKeys);
}



?>
