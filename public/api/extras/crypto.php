#!/usr/bin/env php
<?php
require('vendor/autoload.php');
use Defuse\Crypto\Exception;
use Defuse\Crypto\Key;
use Defuse\Crypto\Crypto;


$key = loadEncryptionKeyFromConfig();
//echo "SECRET KEY: " . $key ."\n";
$secret_data = "Hamlet$562$34h5kjrhdgkjh243";
echo "SECRET DATA: " . $secret_data."\n";
$ciphertext = Crypto::encrypt($secret_data, $key);
echo "CRYPTD DATA: " . $ciphertext."\n";

try {

    $secret_data = Crypto::decrypt($ciphertext, $key);
    echo "SECRET DATA: " . $secret_data;
}
catch (\Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException $ex) {
    // An attack! Either the wrong key was loaded, or the ciphertext has
    // changed since it was created -- either corrupted in the database or
    // intentionally modified by Eve trying to carry out an attack.

    // ... handle this case in a way that's suitable to your application ...
}
catch(Exception $e)
{
$e.printStackTrace();
}

function loadEncryptionKeyFromConfig()
{
    $config   = parse_ini_file("/home/ird/key.ini");
    $keyAscii = $config["key"];
        echo "SECRET KEY: " . $keyAscii ."\n";

        try
        {


                return Key::loadFromAsciiSafeString($keyAscii);
        //return $keyAscii;
        }
        catch(Exception $e)
{
        exit(1);
        $e.printStackTrace();
}

}
?>