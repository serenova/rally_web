<?php
/*
This file is used only for validation of whichever user is attempting to log in.
Making sure that the format of the request meets the standards of the application.
 */

//Public directory
$rootDir = $_SERVER['DOCUMENT_ROOT'];
//Private directory
$parentDir =   dirname($rootDir);
//Load Database functions
require_once("$ROOT" . "/api/auth/AuthFunctions.php");

//Check if POST is sent with JSON object instead, if so pull it into POST
if(empty($_POST))
{
    $_POST = json_decode(file_get_contents('php://input'), true);
}


//Variables that give metadata on the validity of the incoming request:
//---------------------------------------------------------------
//Users return code matches the entry in the database
$userId         =   null;
//Request has all required/allowed Keys
$requestValid   =   false;
//The request is coming from a mobile device as captcha_token is verified.
$userMobile     =   false;



//Check that it has the minimum keys required and sanitize
if((!empty($_POST)) && verifyKeys($_POST))
{
    $requestValid  =   true;
}
//Incorrect request structure. Abort.
else
{
    return;
}

//If entering this statement, normally, the user is sending request from mobile device.
if(isset($_POST["hmac"])&& $_POST["hmac"] !="")
{
    //HMAC is only used with a mobile interface as of now, and should only be checked on this occasion.
    //Used to ensure query is unaltered and secret key used ensures it came from trusted source.
    if($userMobile = verifyHMAC($_POST))
    {
        //HMACS match. Suggests untampered mobile request.
        if(isset($_POST["return_code"])&& $_POST["return_code"] !="")
        {
            $userId   =   $auth->verifyAuthInfo(explode("$", $_POST["return_code"]));
        }

    }
    else
    {
        //If a user attempts to masquerade as a mobile device.
        //If captcha_token and hmac aren't verified, make request invalid.
        $requestValid   = false;
    }
    return;
}

//If using web browser, cookie will maintain that the user is authenticated and trustworthy
else if(isset($_COOKIE["CODE"]))
{
    $userId   =   $auth->verifyAuthInfo(explode("$", $_COOKIE["CODE"]));
}

//END Script
//------------------------------------------------------
//BEGIN Functions

//Verify if all keys are standard rally keys
function verifyKeys($data)
{
    $requiredKeys = array("action"=> 1, "timestamp"=> 1);
    $allowedKeys = array("action"=>1, "timestamp"=>1, "query_values"=>1, "captcha_token"=>1, "return_code"=>1, "hmac"=>1);

    //Returns true if all three required keys are in POST
    if(sizeof(array_intersect_key($data, $requiredKeys))==2)
    {
        //Ensure the request is no more than 10 seconds from time sent
        if(($data["timestamp"]+10)< time())
        {
            return false;
        }
        //Checks if the request includes key values that are not accepted as standard
        if(!array_diff_key($data, $allowedKeys))
        {
            return true;
        }
    }

    return false;
}

//Check if HMAC is as it should be
function verifyHMAC($data)
{
    //Secret HMAC key.
    $key = "jTsTJYFK7HsPQfnK9Rvsh6n2Rpw4yHC";
    //Extract HMAC code
    $hmacProvided   =   $data["hmac"];
    //Remove HMAC entry to compare original data.
    unset($data["hmac"]);

    //Get string in JSON
    $dataString = json_encode($data);

    //Calculate HMAC of data
    $hmacCalculated = bin2hex(hash_hmac('sha256', $dataString, $key,true));
    //Compare the hashes
    if(hash_equals($hmacCalculated, $hmacProvided))
    {
        return true;
    }

    return false;

}
/*
account_status:
0 = registered and logged in but not email Verified
1 = registered and email Verified


*/
?>
