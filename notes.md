**Sleek Auth:**
* return_code as cookie to login user upon a database call which returns USER_ID
* SESSIONs are held on Redis server.
* Upon each call, user_id gets inserted into query_values and the function continues.

* When session expires, return_code is also sent with POST request, which generates another session ID.

* Always send return_code at all times with every request, even when PHPSESSID is included.

* SESSION assures persistent user management.
    - Separate Redis Server for Session management for scalability (Web/mobile)
    - Can block access instantly to a logged in device if necessary
* Once user is validated, create session["user_id"];
* Once Session["user_id"] is made, no longer need to verifyReturnCode
* If request returns error code -1, user is logged out. Return app to login page.


TODO:
* Someone who didn't verify email and is locked out
* Ensure proper handling of user with unverified email (in code and out)
* Renew expiration on cookie/return_code at what time? Do math and renew 1 day prior to expiration in validate
* Implement "Log out of all other devices." -> reset auth_info, send new info back, and update session id cookie

**Sleek Auth - return_code**
* return_code = selector.validator
    - selector = randomNum()
    - validator = encrypt(selector);
    - Database stores hashed version of validator.
* Info held in session/local storage
* Every http request triggers a DB lookup to determine the user_id
* Session-less REST architecture.

PROS:
* Allows individual access to user authentication.
    - Able to redact login access for that particular return_code.
    JWT will invalidate all tokens if encryption key changes.
* Immediate logout capability of the user.
* Selector.validator scheme (validator hashing) helps prevent mass user info hijacking in the event of the DB table info leaking out or being stolen.
* Selector.validator also help prevent timing leaks during DB lookups as opposed to:
    - **SELECT * FROM auth_tokens WHERE token = 'sdfhg384duou8923';**
* Prevents the leakage of the database user_id field which leaks the number of active users on service.


CONS:
    * Speed?
        - Maybe DB calls to read user_id (using return_code) take longer than decrypting JWTs.
        But this seems like a negligible difference in timing.



**Sleek Auth JWT:**

PROS:
    * Simple and proven

CONS:
    * Decryption on each http request = computing intensive
    * No ability for immediate user logout in the event of security breach or user token theft.
        - Maybe fixable by maintaining UUID per device and verifying every so often?


------------------------------------
Conclusion:
* The security advantage of instantaneous logout, wins over the disadvantage of arguably arbitrarily faster validation per HTTP request with JWT tokens. Also if JWTs need to be used, only the Validation file needs to change, and that is trivial.


Progress
    * Validate.php should be run every time a call to API is made    

Future
* Return code should be dependent on time() as well.
* OAuth2.0

return_code:

1. Only given new one during sign in
2. Checked every api call
3. Database holds code_renewal_time, code_expiry_time
    * In verifyReturnCode, if renewal time is passed, extend, both renewal time and expiry time by 1 week.
4.

API calls:
CookieHandler.setDefault(new CookieManager());
CookieManager cookieManager = new CookieManager(new PersistentCookieStore(mContext),
    CookiePolicy.ACCEPT_ORIGINAL_SERVER);
CookieHandler.setDefault(cookieManager);

1. Secret Code store in app (concat with email and encoded with Cipher) (Used for registration verification/captcha)
    * encrypt(secret + email) -> send to server data["verifyCaptcha"]
    * encrypt(secret + email) see if match(data["verifyCaptcha") -> register user
2. return_Code calculated with another secret code: php -> encrypt(otherSecret + userinfo)
2. Login, get return_code (will be used as verification for future API calls)
2. Send return_code with every request. check database Along with MAC (message Auth code)
3. timestamp code is also sent with every request to make sure request was sent within reasonable time span
3.
//This process tries to prevent man-in-the-middle attacks and provides user authentication from every API call
//Validate.php must be included in every call that is intended to be made by a logged in user.
Verification Process (Logged in API call)
    1. Decrypt HMAC -> explode("$", $hmac) -> $timestamp, $userID, $mac
    2. Verify that timestamp is earlier than 5 minutes from time();
    3. Verify that $mac equals the hash of $data["query_values"]
    4. Verify that $userID is logged in
    5. Then make $userVerified=true

//This process tries to prevent man-in-the-middle attacks and provides user authentication from every API call
//Validate.php must be included in every call that is intended to be made by a logged in user.
New Verification Process, returns $userVerified:
    1. Hash sha512(query_values + timestamp + action + returnCode + hash(secret))
    2. Generate hash from $POST and secret
    3. If verified, continue in making sure user isLoggedIn, by decrypting returnCode, and getting userID, and querying database.


Only thing to be Encrypted is return_code
Attempt Hashing prior to encryption    
Encryption done on php should use defuse/php-encryption
Encryption done between platforms should use RNCryptor

Add date_last_loggedIn
Uses:
    Return Code: Encryption just for extra obscurity, and for practice
    Checked on every call to server to verify user/login
    Option 1:
        Provides less back and forth database queries, more security,
        code = id$email$MOBILE$expirydate  WEB    
        Is decrypted in every call to get id and email for query

    Option 2:
        MD5, no encryption
        id and email are sent with query values

    captchaToken: Used to "verify" user is from mobile app (not using cookies)


    Need to find when to update expiry_date
Scenarios:

Register-Mobile:
Keys:   action, query_values, captchaToken, timestamp, hmac = md5(secret key + action, query_values, timestamp)
    1. Validate.php (action=register)
    2. Check to see if it has minimum entries (email, password), if not, send error
    3. Check to see if it has any keys that don't match key index for rally calls (action, timestamp, query values) Hackers might put extra keys.
    4. Goes to authenticate.php, captchaToken=md5(secretkey1+timestamp+email) verify mobile
    5. Fully registers

//How to prevent user from trying to login over and over without return code
//It will stress the system

Login-Mobile (Request for a new return_code):
Keys: action, query_Values, timestamp, hmac
    1. Validate
    2. ""
    3. ""
    4. Update authenticate.php for login case, check for mobile captcha, add type=MOBILE
    4. Verify that they're not logged in (might need to replace $verified in userLogin with just isLoggedIn). Actually it doesn't even matter if they're logged in, who cares, apparantly they didn't send the return code, so something's fucked. Either way, just go ahead and get another return code, as long as a perceived attack is not happening
    5. If the timestamp between last_login is less than 3 seconds, do not allow.This will attempt to prevent automated stress attacks.
    6. Otherwise, login

Logout
    1. Validate
    2. authenticate.php, decrypt return_code, add user_id and email to query values
    3.  


    *The Config file for the database credentials is held outside the server root folder for security.
    *The database queries must be escape to prevent sql injection.
    *The DB_Connect file has a static $connection variable to make sure that the connection will remain for all calls to DB_Connect().
    *When using 'require_once() etc...' realize that the reference point document root is from the location of the php file first called.


****As extra precation, don't let calls go through if the time() is past expiry date
