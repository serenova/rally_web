<?php

require_once 'include/DB_Functions.php';
$db = new DB_Functions();

// array for final json respone
$response = array();
$parentDirectory = realpath(__DIR__ . '/..');

$latitude 	= $_POST['latitude'];
$longitude 	= $_POST['longitude'];
$sector     = $_POST['sector'];
$user_name   = $_POST['user_name'];
$user_id     = $_POST['user_id'];
$public     = $_POST['public'];


if($public == '0')
{
	$path = $parentDirectory.'/user/'. $user_id ;
}
else
{
	$path = $parentDirectory.'/sector/'. $sector ;
}

if (!file_exists($path) && !is_dir($path))
{
	mkdir($path, 01777, true);
}

// getting server ip address
$server_ip = gethostbyname(gethostname());

// final file url that is being uploaded
$file_upload_url = 'http://' . $server_ip . $path;


if (isset($_FILES['image']['name'])) {

	$fileName   = basename($_FILES['image']['name']);
	$path = $path . '/' . $fileName;

	// reading other post parameters
	$email = isset($_POST['email']) ? $_POST['email'] : '';
	$website = isset($_POST['website']) ? $_POST['website'] : '';

	$response['file_name'] = $fileName;
	$response['email'] = $email;
	$response['website'] = $website;

	try {
		// Throws exception incase file is not being moved
		if (!move_uploaded_file($_FILES['image']['tmp_name'], $path)) {
			// make error flag true
			$response['error'] = true;
			$response['message'] = 'Could not move the file!';
		}
		$stored = $db->storeToDatabase($user_id, $user_name, $latitude, $longitude, $sector, $public, $fileName);
		// File successfully uploaded
		$response['stored'] = $stored;
		$response['message'] = 'File uploaded successfully!';
		$response['error'] = false;
		$response['file_path'] = $file_upload_url . basename($_FILES['image']['name']);
	} catch (Exception $e) {
		// Exception occurred. Make error flag true
		$response['error'] = true;
		$response['message'] = $e->getMessage();
	}
}
else {
	// File parameter is missing
	$response['error'] = true;
	$response['message'] = 'Not received any file!';
}

// Echo final json response to client
echo json_encode($response);


?>
