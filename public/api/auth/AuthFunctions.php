<?php

//Public directory
$rootDir    =   $_SERVER['DOCUMENT_ROOT'];
//Load Required Documents/Functions
require_once("$rootDir" . "/api/db/DbFunctions.php");
require_once("$rootDir" . "/api/security/SecurityFunctions.php");

class AuthFunctions
{
    //Amount of hours the code is from expiring.
    const RENEWAL_MARGIN = 43200;
    private $db;
    // constructor
    function __construct() {
        $this->db = new DbFunctions();
    }

    // destructor
    function __destruct() {

    }
    //Quick check with email to see if user has already registered
    function userExists($data)
    {
        //Create bind for read attempt
        $readTable          = "users";
        $readWhere          = "email=:email";
        $readBind[":email"] = $data["email"];

        //Check to see if user already exists
        if($this->db->read($readTable, $readWhere, $readBind))
        {
            return true;
        }

        return false;
    }
    function createAuthInfo($userId, $returnCode)
    {
        $authInfo = explode("$", $returnCode);
        //Return code will expire 1 day after this moment.
        $expireTime         =   time()+86400;   //60*60*24 = 86400 Seconds
        $renewTime          =   $expireTime;    //This will not allow user to renew their return_code

        $table                              =   "auth_info";
        $authData["selector"]               =   $authInfo[0];
        $authData["hashed_validator"]       =   hash("sha256", $authInfo[1]);
        $authData["renew_time"]             =   $renewTime;
        $authData["expire_time"]            =   $expireTime;
        $authData["user_id"]                =   $userId;

        //Attempt to create and auth info entry for the user 5 times if collisions occur.
        //After 5, Declare a serious error
        for($x=0; !$this->db->create($table, $authData)&&($x<5); $x++)
        {
            $authData["selector"]               =   substr(uniqid(rand(), true), 0, 12);
        }

        if($x>=5)
        {
            return false;
        }

        $updateTable                            =   "users";
        $updateValues["auth_id"]                =   $this->db->getLastId();
        $updateWhere                            =   "user_id=:user_id";
        $updateBind[":user_id"]                 =   $userId;

        //Update user to have its associated auth_id
        $query  =   $this->db->update($updateTable,$updateValues, $updateWhere, $updateBind);
        //print_r($updateValues);
        //print_r($query);
        //If confirmation is verified, return success
        if($query->rowCount())
        {
            return true;
        }

        return false;

    }
    //Extend sign in session to further date
    function updateRenewExpireTime($userId, $renewalTime, $expireTime)
    {
        $updateTable                            =   "auth_info";
        $updateValues["renew_time"]             =   $renewalTime;
        $updateValues["expire_time"]            =   $expireTime;
        $updateWhere                            =   "user_id=:user_id";
        $updateBind[":user_id"]                 =   $userId;

        //update renewal and expiry time for return_code of user
        $query  =   $this->db->update($updateTable,$updateValues, $updateWhere, $updateBind);

        //If confirmation is verified, return true
        if($query->rowCount())
        {
            return true;
        }
        return false;
    }
    function getUserReturnCode($userId)
    {
        //Get the user's info
        $readTable              = "auth_info";
        $readWhere              = "user_id=:user_id";
        $readBind[":user_id"]   = $userId;
        $readFields             = "selector";

        //Obtain the user's id and password hash
        $query          = $this->db->read($readTable, $readWhere, $readBind, $readFields);
        //If query returned positive and there's only one user, continue to login
        if($query)
        {
            $query = $query[0];
            //return_code
            return $query["selector"].encrypt($query["selector"]);
        }

    }

    //Register new user
    function userRegister($data)
    {
        //Create default result of failure
        $result["success"]  =   0;

        //Initialize new user info. date_updated will change only for user info changes (password, username, etc..)
        $data["date_created"]   =   time();
        $data["date_updated"]   =   $data["date_created"];
        $data["password"]       =   password_hash($data['password'], PASSWORD_DEFAULT);
        $data["account_status"] =   0;


        //Create new user
        if($this->db->create("users", $data))
        {
            //Get user id and create a return code for Auth Info
            $userId =   $this->db->getLastId();
            $returnCode = createReturnCode();

            //Attempt to create an auth info entry for user
            if($this->createAuthInfo($userId, $returnCode))
            {
                //Create the result object that will be sent to caller of function.
                $responseData["email"]      =   $data["email"];
                $responseData["user_id"]    =   $userId;
                $responseData["return_code"]=   $returnCode;

                $result["success"]      =   1;
                $result["data"]         =   $responseData;
                $result["comment"]      =  "Created New User";
            }
            else
            {
                $result["comment"]  =   "Could not update auth info for user ".$userId;
            }
        }
        return $result;
    }


    //Sign in the user
    function userSignIn($data)
    {
        //Create default result of failure
        $result["success"]  =   0;

        //Get the user's info
        $readTable          = "users";
        $readWhere          = "email=:email AND account_status=1";
        $readBind[":email"] = $data["email"];
        $readFields         = "user_id, password";

        //Obtain the user's id and password hash
        $query          = $this->db->read($readTable, $readWhere, $readBind, $readFields);

        //If query returned positive and there's only one user, continue to login
        if($query)
        {
            //Get the only row entry in the query result
            $query      = $query[0];

            $userId         = $query["user_id"];
            $passwordHash   = $query["password"];

            //Verify database password hash with argument password input
            //If password is correct, update the entry of the user on the database
            if(password_verify($data['password'], $passwordHash))
            {
                //Create unique code user will use to login without typing user and pass (Cookie or Session on browser environments)
                $expireTime         =   time()+604800;  //One week 60*60*24*7 = 604800 seconds
                $renewalTime        =   $expireTime-86400;
                if($this->createAuthInfo($userId, $returnCode =  createReturnCode()))
                {
                    //Give the user their return code
                    $responseData["return_code"]    =   $returnCode;
                    $responseData["user_id"]        =   $userId;
                    $result["data"]     =   $responseData;
                    $result["success"]  =   1;
                    $result["comment"]  =   "Successfully logged in.";
                }

            }
            else
            {
                $result["comment"]     =   "Wrong Password.";
            }

        }

        return $result;
    }
    //Keep different functions (Facebook and Google) in case changes must be made to different login methods
    function userFBAuthenticate($data)
    {
        //If User Exists
        if(userExists($data))
        {
            return userSignIn($data);
        }
        else
        {
            //If user is new
            return userRegister($data);
        }

    }
    //Keep different functions (Facebook and Google) in case changes must be made to different login methods
    function userGGAuthenticate($data)
    {

        //If User Exists
        if(userExists($data))
        {
            return userSignIn($data);
        }
        else
        {
            //If user is new
            return userRegister($data);
        }
    }

    function userSignOut()
    {
        //Default failure result
        $result["success"]  =   0;

        if(isset($_COOKIE["CODE"]))
        {
            //Destroy cookie by setting the time to 1 (The year 1970)
            setcookie("CODE", " ", 1, "/", "serenonva.xyz");
        }
        if(isset($_COOKIE["PHPSESSID"]))
        {
            //Destroy cookie by setting the time to 1 (The year 1970)
            setcookie("PHPSESSID", " ", 1, "/", "rally.serenonva.xyz");

        }

        //Doesn't return a value so was unable to place in if statement.
        session_unset();
        // Destroy the session variables, then destroy session
        if(session_id() && session_destroy())
        {
            $result["success"]  =   1;
            $result["comment"]  =   "Successfully logged out.";

        }
        else
        {
            $result["success"]  =   0;
            $result["comment"]     =   "Unable to destroy session.";
        }

        return $result;
    }
    //Returns User ID if return_code is valid. False Otherwise.
    function verifyAuthInfo($authInfo)
    {
        $result = false;
        //Get the user's info
        $readTable          = "auth_info";
        $readWhere          = "selector=:selector";
        $readBind[":selector"] = $authInfo[0];
        $readFields         = "hashed_validator,user_id,renew_time,expire_time";

        //Obtain the expiry_time of the users session
        $query          = $this->db->read($readTable, $readWhere, $readBind, $readFields);
        //print_r($query);
        if(sizeof($query)==1)
        {
            $query = $query[0];
            $now = time();
            //Affirm that the user is using valid return_code
            if(hash_equals($query["hashed_validator"],hash("sha256",$authInfo[1])) && $now<$query["expire_time"])
            {
                $result = $query["user_id"];
                //Need to extend the validity of the user's auth info
                if(($query["expire_time"]-$now)<self::RENEWAL_MARGIN)
                {
                    $this->updateAuthInfo($query["user_id"]);
                }
                //Code is expired. Delete it from database
                else if($now>$query["expire_time"])
                {
                    $this->removeExpiredAuth($query, $now);
                    $result = false;
                }
            }

        }

        return $result;

    }
    function removeExpiredAuth($data, $now)
    {
        $deleteTable                =   "auth_info";
        $deleteWhere                =   "selector=:selector AND expire_time<:now";
        $deleteBind[":selector"]    =   $data["selector"];
        $deleteBind[":now"]         =   $now;

        $query  =   $this->db->delete($deleteTable, $deleteWhere, $deleteBind);

    }
    //Update the return code of user with given userId
    function updateAuthInfo($userId, $authInfo)
    {

        $updateTable                            =   "auth_info";
        $updateValues["selector"]               =   $authInfo[0];
        $updateValues["hashed_validator"]       =   $authInfo[1];
        $updateValues["renew_time"]             =   $authInfo[2];
        $updateValues["expire_time"]            =   $authInfo[3];

        $updateWhere                            =   "user_id=:user_id";
        $updateBind[":user_id"]                 =   $userId;

        //Update auth info
        $query  =   $this->db->update($updateTable,$updateValues, $updateWhere, $updateBind);

        //If confirmation is verified, return true
        if($query->rowCount())
        {
            //Return USER_ID
            return true;
        }
        return false;

    }
    function isEmailVerified($data)
    {
        //Query database to see if user is in database;
        $readWhere  =   "email=:email AND account_status=1";
        $readBind[":email"]         =   $data["email"];
        $readFields =   "user_id";

        $query      =   $this->db->read("users", $readWhere, $readBind, $readFields);

        if($query)
        {
            return true;
        }
        return false;
    }
    //Activate the user after they click the verification link
    function activateAccount($data)
    {

        $authInfo   =   explode("$", $data["return_code"]);
        $userId     =   $this->verifyAuthInfo($authInfo);

        if($userId)
        {
            $updateTable                            =   "users";
            $updateValues["account_status"]         =   1;
            $updateValues["date_updated"]           =   time();
            $updateWhere                            =   "user_id=:user_id AND account_status=0";
            $updateBind[":user_id"]                 =   $userId;

            //Update account_status to 1 from 0. Official registration confirmation.
            $query  =   $this->db->update($updateTable,$updateValues, $updateWhere, $updateBind);

            //If confirmation is verified, return success
            if($query->rowCount())
            {
                return true;
            }

            return false;
        }
    }
}
?>
