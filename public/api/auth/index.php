<?php
//public directory
$ROOT = $_SERVER['DOCUMENT_ROOT'];
$rootParent =   dirname($ROOT);

require_once("$ROOT" . "/api/auth/AuthFunctions.php");

//You must validate/sanitize the HTTP request
require_once("$ROOT" . "/api/validate.php");

//Load composer autoloader
require_once($rootParent.'/lib/vendor/autoload.php');

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// include db handler
$auth = new AuthFunctions();


/*
Every response will be in JSON and will have ["success"] and ["data"].
Supplementary data will be added in increments. ["data_1"], ["data_2"] and so on.
 */

$failureResponse            = array();
$failureResponse["success"] = 0;
$failureResponse["comment"]    = "Incorrect input";



/**
 * File to handle requests for user authentication
 * Accepts POST
 *
 * Each request will be identified by ACTION
 * Response will be JSON data
 *
 * Find way to prevent DDOS attack or failed multiple access attempts
 */
print_r(json_encode($_POST));
echo "\r\n";
echo "REQUESTVALID: ".$requestValid."\r\n";
echo "USERVERIFIED: ".$userId."\r\n";
echo "USERMOBILE: ".$userMobile."\r\n";


//Default response is a failure
$response = $failureResponse;

//A request was made to login, logout, or register a user.
if($requestValid)
{
    // get action
    switch ($_POST["action"])
    {
        //Use access token and secret_proof to verify real user.
    case "fb_authenticate":
        $response = $auth->userFBAuthenticate($_POST["query_values"]);
        break;
        //Use access token to verify real user
    case "gg_authenticate":
        $response = $auth->userGGAuthenticate($_POST["query_values"]);
        break;
    case "sign_in":
        //Ensure that the user is not signed in before attempting sign in.
        if(!$userId && isset($_POST["query_values"]["email"]) && isset($_POST["query_values"]["password"]))
        {
            //If called from browser, the userSignIn function will setCookie.
            $response = $auth->userSignIn($_POST["query_values"]);
            if($response["success"] && !$userMobile)
            {
                $_SESSION["user_id"] = $response["data"]["user_id"];
                //Set cookie with user return code and expiration of 1 week
                setcookie("CODE", $response["data"]["return_code"], time()+86400, "/", "serenova.xyz");
            }
        }
        else if($userId)
        {
            $response["success"]    =   1;
            $response["comment"]    =   "Already Signed In.";
        }
        break;
    case "sign_out":
        $response = $auth->userSignOut();
        break;
    case "register":
        //Mobile or Web Captcha Registration Verification prior to query
        if ($userMobile || (isset($_POST["g-recaptcha-response"]) && verifyGoogleRecaptcha($_POST["g-recaptcha-response"])) )
        {
            if(!($auth->userExists($_POST["query_values"])==true))
            {
                $response = $auth->userRegister($_POST["query_values"]);
                if($response["success"])
                {
                    //Remove user_id when giving back to user
                    unset($response["data"]["user_id"]);
                    //Send email to verify account
                    sendVerificationEmail($response["data"]);
                    if($userMobile)
                    {
                        //Attempt to hide personal info
                        unset($response["data"]["email"]);
                    }
                    else
                    {
                        //Set cookie with user return code and expiration of 1 week
                        setcookie("CODE", $response["data"]["return_code"], time()+86400, "/", "serenova.xyz");
                        //Hide data because only cookie is needed
                        unset($response["data"]);
                    }
                }
            }
        }
        break;
    case "test":
        $response["success"]	=	1;
        $response["data"]   	=   	"IN TEST";
        //$response                   = $failureResponse;
        break;
    default:
        $response["data"] = "IN FB";
        break;
    }
}


echo json_encode($response);

function sendVerificationEmail($data)
{

    $mail = new PHPMailer(true);
    try
    {
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output = 2
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'mail.privateemail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'hamlet@serenova.xyz';                 // SMTP username
        $mail->Password = 't)Vy5zUiASpK';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('hamlet@serenova.xyz', 'Rally');
        $mail->addAddress('hamletfasliu@gmail.com', 'Hamlet Fasliu');     // Add a recipient

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'User Verification';

        //Content is located in the configuration folder of the project.
        //The related query values are inserted into the content before sending the email.
        $mail->Body    = str_replace("@",http_build_query($data), file_get_contents("/var/www/rally_web/config/verify_email.html"));

        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        //Replace the success page with a nice bootstrap page
        echo 'Message has been sent';

    }
    catch (Exception $e)
    {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    }
}
/*
This function is used to verify the recaptcha token with Google, who provided by the recaptcha.
 */
function verifyGoogleRecaptcha($recaptchaToken)
{


    $url      = "https://google.com/recaptcha/api/siteverify";
    $bodyData = array();

    //BODY
    $bodyData["secret"]   = "6LdAxBIUAAAAAG7ai_FZvUUTTmhWaFFCEYsF8ON5";
    $bodyData["response"] = $recaptchaToken;
    $bodyData["remoteip"] = $_SERVER["REMOTE_ADDR"];


    //Get cURL resource
    $curl = curl_init();

    //SET REQUEST OPTIONS
    $curlOptions                         = array();
    $curlOptions[CURLOPT_RETURNTRANSFER] = 1;
    $curlOptions[CURLOPT_URL]            = $url;
    //$curlOptions[CURLOPT_USERAGENT]         = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2";
    $curlOptions[CURLOPT_POST]           = 1;
    $curlOptions[CURLOPT_POSTFIELDS]     = $bodyData;
    $curlOptions[CURLOPT_IPRESOLVE]      = CURL_IPRESOLVE_V4;

    curl_setopt_array($curl, $curlOptions);

    //Execute HTTP Request
    $resp = curl_exec($curl);
    // Close request to clear up some resources
    echo $resp;
    curl_close($curl);
    $resp = json_decode($resp, true);
    echo "GOOGLE RESPONSE: ";
    print_r($resp);
    if ($resp["success"]) {

        return true;
    } else {
        return false;
    }


}






?>
